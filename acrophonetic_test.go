package acrophonetic

import "testing"

func TestRegister(t *testing.T) {
	// Check register with a transFn that should fulfill the Transcriber
	// interface
	Register(transFn(func(r rune) string {
		return ""
	}))

	// Check the transcriber
	if transcriber == nil {
		t.Error("Expected transcriber to be set")
	} //if
} //func

func TestRegisterFunc(t *testing.T) {
	// Check register with a transFn that should fulfill the Transcriber
	// interface
	RegisterFunc(func(r rune) string {
		return ""
	})

	// Check the transcriber
	if transcriber == nil {
		t.Error("Expected transcriber to be set")
	} //if
} //func

func TestTransRune(t *testing.T) {
	r := 'a'

	// Check TransRune without a Transcriber
	if expected, actual := "", TransRune(r); actual != expected {
		t.Errorf(`Expected "%s" but was "%s"`, expected, actual)
	} //if

	rs := "Ay"

	transcriber = transFn(func(r rune) string {
		return rs
	})

	// Check TransRune without a Transcriber
	if expected, actual := rs, TransRune(r); actual != expected {
		t.Errorf(`Expected "%s" but was "%s"`, expected, actual)
	} //if

} //func

func TestSpellSlice(t *testing.T) {
	// Make sure the transcriber isn't set
	transcriber = nil

	// The test slice
	test := []rune{
		'a', 'b', 'c',
	}

	// Spell the slice
	done := SpellSlice(test)

	// Make sure the lengths are the same
	if expected, actual := 0, len(done); actual != expected {
		t.Errorf("Expected the returned slices to be %d but was %d %v",
			expected, actual, done)
	} //if

	// Set a transcriber function
	transcriber = transFn(func(r rune) string {
		switch r {
		case 'a':
			return "a"
		case 'b':
			return "b"
		case 'c':
			return "C"
		} //switch
		return ""
	})

	// Spell the slice again
	done = SpellSlice(test)

	// Make sure the lengths are the same
	if expected, actual := len(test), len(done); actual != expected {
		t.Fatalf("Expected the returned slices to be %d but was %d",
			expected, actual)
	} //if

	// Make sure the runes are translated according to the transcriber
	for i, c := range test {
		if expected, actual := TransRune(c), done[i]; actual != expected {
			t.Errorf(`Expected %c to be "%s" but was "%s"`,
				c, expected, actual)
		} //if
	} //for
} //func

func TestSpell(t *testing.T) {
	// Set a transcriber function
	transcriber = transFn(func(r rune) string {
		switch r {
		case 'a':
			return "a"
		case 'b':
			return "b"
		case 'c':
			return "C"
		} //switch
		return ""
	})

	// The test string
	test := []rune("abc")

	// Spell the test string
	done := Spell(string(test))

	// Check the characters
	for i, c := range test {
		if expected, actual := TransRune(c), done[i]; actual != expected {
			t.Errorf(`Expected %c to be "%s" but was "%s"`,
				c, expected, actual)
		} //if
	} //for
} //func
