package acrophonetic

// Transcriber translates a rune to a a representation.
type Transcriber interface {
	TransRune(r rune) string
} //interface

type transFn func(r rune) string

func (t transFn) TransRune(r rune) string {
	return t(r)
} //func

func TransFunc(f func(r rune) string) Transcriber {
	return transFn(f)
} //func
