package morse

import "testing"

func TestTransRune(t *testing.T) {
	trepr := func(s string, rs ...rune) {
		for _, r := range rs {
			if expected, actual := s, TransRune(r); actual != expected {
				t.Errorf(`Expected "%s" but was "%s" for %c"`,
					expected, actual, r)
			} //if
		} //if
	} //func

	trepr(dot+dash, 'a', 'A')
	trepr(dash+dot+dot+dot, 'b', 'B')
	trepr(dash+dot+dash+dot, 'c', 'C')
	trepr(dash+dot+dot, 'd', 'D')
	trepr(dot, 'e', 'E')
	trepr(dot+dot+dash+dot, 'f', 'F')
	trepr(dash+dash+dot, 'g', 'G')
	trepr(dot+dot+dot+dot, 'h', 'H')
	trepr(dot+dot, 'i', 'I')
	trepr(dot+dash+dash+dash, 'j', 'J')
	trepr(dash+dot+dash, 'k', 'K')
	trepr(dot+dash+dot+dot, 'l', 'L')
	trepr(dash+dash, 'm', 'M')
	trepr(dash+dot, 'n', 'N')
	trepr(dash+dash+dash, 'o', 'O')
	trepr(dot+dash+dash+dot, 'p', 'P')
	trepr(dash+dash+dot+dash, 'q', 'Q')
	trepr(dot+dash+dot, 'r', 'R')
	trepr(dot+dot+dot, 's', 'S')
	trepr(dash, 't', 'T')
	trepr(dot+dot+dash, 'u', 'U')
	trepr(dot+dot+dot+dash, 'v', 'V')
	trepr(dot+dash+dash, 'w', 'W')
	trepr(dash+dot+dot+dash, 'x', 'X')
	trepr(dash+dot+dash+dash, 'y', 'Y')
	trepr(dash+dash+dot+dot, 'z', 'Z')
	trepr(dot+dash+dash+dash+dash, '1')
	trepr(dot+dot+dash+dash+dash, '2')
	trepr(dot+dot+dot+dash+dash, '3')
	trepr(dot+dot+dot+dot+dash, '4')
	trepr(dot+dot+dot+dot+dot, '5')
	trepr(dash+dot+dot+dot+dot, '6')
	trepr(dash+dash+dot+dot+dot, '7')
	trepr(dash+dash+dash+dot+dot, '8')
	trepr(dash+dash+dash+dash+dot, '9')
	trepr(dash+dash+dash+dash+dash, '0')
} //func
