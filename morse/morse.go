package morse

import "bitbucket.org/antizealot1337/acrophonetic"

const (
	dot  = "·"
	dash = "-"
)

func init() {
	Use()
} //func

func TransRune(r rune) string {
	switch r {
	case 'a', 'A':
		return dot + dash
	case 'b', 'B':
		return dash + dot + dot + dot
	case 'c', 'C':
		return dash + dot + dash + dot
	case 'd', 'D':
		return dash + dot + dot
	case 'e', 'E':
		return dot
	case 'f', 'F':
		return dot + dot + dash + dot
	case 'g', 'G':
		return dash + dash + dot
	case 'h', 'H':
		return dot + dot + dot + dot
	case 'i', 'I':
		return dot + dot
	case 'j', 'J':
		return dot + dash + dash + dash
	case 'k', 'K':
		return dash + dot + dash
	case 'l', 'L':
		return dot + dash + dot + dot
	case 'm', 'M':
		return dash + dash
	case 'n', 'N':
		return dash + dot
	case 'o', 'O':
		return dash + dash + dash
	case 'p', 'P':
		return dot + dash + dash + dot
	case 'q', 'Q':
		return dash + dash + dot + dash
	case 'r', 'R':
		return dot + dash + dot
	case 's', 'S':
		return dot + dot + dot
	case 't', 'T':
		return dash
	case 'u', 'U':
		return dot + dot + dash
	case 'v', 'V':
		return dot + dot + dot + dash
	case 'w', 'W':
		return dot + dash + dash
	case 'x', 'X':
		return dash + dot + dot + dash
	case 'y', 'Y':
		return dash + dot + dash + dash
	case 'z', 'Z':
		return dash + dash + dot + dot
	case '1':
		return dot + dash + dash + dash + dash
	case '2':
		return dot + dot + dash + dash + dash
	case '3':
		return dot + dot + dot + dash + dash
	case '4':
		return dot + dot + dot + dot + dash
	case '5':
		return dot + dot + dot + dot + dot
	case '6':
		return dash + dot + dot + dot + dot
	case '7':
		return dash + dash + dot + dot + dot
	case '8':
		return dash + dash + dash + dot + dot
	case '9':
		return dash + dash + dash + dash + dot
	case '0':
		return dash + dash + dash + dash + dash
	} //switch
	return ""
} //func

func Use() {
	acrophonetic.RegisterFunc(TransRune)
} //func
