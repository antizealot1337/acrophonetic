package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"

	"bitbucket.org/antizealot1337/acrophonetic"
	"bitbucket.org/antizealot1337/acrophonetic/morse"
	"bitbucket.org/antizealot1337/acrophonetic/nato"
	"bitbucket.org/antizealot1337/acrophonetic/usmc"
)

type speller uint8

const (
	NoSpeller speller = iota
	NatoSpeller
	UsmcSpeller
	MorseSpeller
)

func parse(s string) speller {
	switch strings.ToLower(s) {
	case "nato":
		return NatoSpeller
	case "usmc":
		return UsmcSpeller
	case "morse":
		return MorseSpeller
	default:
		return NoSpeller
	} //switch
} //func

func main() {
	sp := flag.String("speller", "nato", "Choose the speller output")

	// Set the usage flag
	flag.Usage = func() {
		fmt.Printf("USAGE: %s MSG\n", os.Args[0])
		fmt.Println("If a message is not provided on the command line then stdin will be read.")
		flag.PrintDefaults()
		fmt.Println("\nAvailable spellers:")
		fmt.Println("\tnato")
		fmt.Println("\tusmc")
		fmt.Println("\tmorse")
	} //func

	// Parse the command line
	flag.Parse()

	switch parse(*sp) {
	case NoSpeller:
		fmt.Println("Unidentified speller")
		os.Exit(-1)
	case NatoSpeller:
		nato.Use()
	case UsmcSpeller:
		usmc.Use()
	case MorseSpeller:
		morse.Use()
	} //if

	// Check if a message was provided on the command line
	if flag.NArg() == 0 {
		// Read from stdin
		stdinPhonetic()
	} else {
		// Combine all the args into a single string
		s := strings.Join(flag.Args(), " ")

		// Get the acrophonetic representation for the entire arg
		// string and convert it to a single string
		s = strings.Join(acrophonetic.Spell(s), " ")

		// Write to the output
		fmt.Println(s)
	} //if

} //func

func stdinPhonetic() {
	// Create a scanner
	scanner := bufio.NewScanner(os.Stdin)

	// Scan for input
	for scanner.Scan() {
		// Get the line
		line := scanner.Text()

		// Write the line converted to its acrophonetic representation
		// (with a single space between the letter)
		fmt.Println(strings.Join(acrophonetic.Spell(line), " "))
	} //for

} //readFromStdin
