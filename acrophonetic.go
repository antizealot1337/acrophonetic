package acrophonetic

import "sync"

var (
	translock   sync.Mutex
	transcriber Transcriber
)

// Register instructes TransRune to use the provided Transcriber.
func Register(t Transcriber) {
	translock.Lock()
	transcriber = t
	translock.Unlock()
} //func

// RegisterFunc instructs TransRune to use the provided function.
func RegisterFunc(f func(r rune) string) {
	translock.Lock()
	transcriber = transFn(f)
	translock.Unlock()
} //func

// TransRune returns acrophonetic representation for the provided rune. It uses
// the provided Transcriber or the default NATO transcriber. If no
// representation exists it an empty string will be returned.
func TransRune(c rune) string {
	translock.Lock()
	defer translock.Unlock()
	if transcriber == nil {
		return ""
	} //if

	return transcriber.TransRune(c)
} //func

// SpellSlice will return the phonetic representation (if any) for all the
// provided runes.
func SpellSlice(r []rune) []string {
	// Create a slice to hold the strings
	ph := make([]string, 0, len(r))

	// Loop through the runes
	for _, c := range r {
		// Convert the rune to the phonetic representation
		if phon := TransRune(c); phon == "" {
			continue
		} else {
			// Add the string to the runes
			ph = append(ph, TransRune(c))
		} //fi
	} //for

	return ph
} //func

// Spell is a convience method. It converts the provided string to a slice of
// runes and calls SpellSlice on them.
func Spell(s string) []string {
	return SpellSlice([]rune(s))
} //func
