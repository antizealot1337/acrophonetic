package usmc

import (
	"bitbucket.org/antizealot1337/acrophonetic"
	"bitbucket.org/antizealot1337/acrophonetic/nato"
)

func init() {
	Use()
} //func

func TransRune(c rune) string {
	// Check if the character is "9"
	if c == '9' {
		return "Niner"
	} //if

	// Defer to nato for translating
	return nato.TransRune(c)
} //func

func Use() {
	acrophonetic.RegisterFunc(TransRune)
} //func
