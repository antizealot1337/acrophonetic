package usmc

import "testing"

func TestTransRune(t *testing.T) {
	transtest := func(c rune, expected string) {
		if actual := TransRune(c); actual != expected {
			t.Error(`Expected "%s" with %c but was "%s"`, expected,
				c, actual)
		} //if
	} //func

	transtest('A', "Alfa")
	transtest('9', "Niner")
} //func
