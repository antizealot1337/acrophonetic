# Acrophonetic
[![GoDoc](https://godoc.org/bitbucket.org/antizealot1337/acrophonetic?status.svg)](https://godoc.org/bitbucket.org/antizealot1337/acrophonetic)

Acrophonetic is a library (and command line program) that transribes characters
in a string to a spelling representation.

It currently spells:
* [NATO alphabet](https://wikipedia.org/wiki/NATO_phonetic_alphabet).
* USMC variant of the NATO alphabet. (9 is "Niner")
* [Morse code](https://wikipedia.org/wiki/Morse_code).

## Status
This library is currently unstable and the API may change.

## License
Licensed under the terms of the BSD license. See LICENSE file for more
infomation.
