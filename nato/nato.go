package nato

import "bitbucket.org/antizealot1337/acrophonetic"

func init() {
	// Register the nato rune translator function as the transcriber function
	Use()
} //init

// TransRune translates the provided rune to the NATO or International
// Radiotelephony Spelling Alphabet representation.
func TransRune(c rune) string {
	switch c {
	case 'a', 'A':
		return "Alfa"
	case 'b', 'B':
		return "Bravo"
	case 'c', 'C':
		return "Charlie"
	case 'd', 'D':
		return "Delta"
	case 'e', 'E':
		return "Echo"
	case 'f', 'F':
		return "Foxtrot"
	case 'g', 'G':
		return "Golf"
	case 'h', 'H':
		return "Hotel"
	case 'i', 'I':
		return "India"
	case 'j', 'J':
		return "Juliett"
	case 'k', 'K':
		return "Kilo"
	case 'l', 'L':
		return "Lima"
	case 'm', 'M':
		return "Mike"
	case 'n', 'N':
		return "November"
	case 'o', 'O':
		return "Oscar"
	case 'p', 'P':
		return "Papa"
	case 'q', 'Q':
		return "Quebec"
	case 'r', 'R':
		return "Romeo"
	case 's', 'S':
		return "Sierra"
	case 't', 'T':
		return "Tango"
	case 'u', 'U':
		return "Uniform"
	case 'v', 'V':
		return "Victor"
	case 'w', 'W':
		return "Whiskey"
	case 'x', 'X':
		return "Xray"
	case 'y', 'Y':
		return "Yankee"
	case 'z', 'Z':
		return "Zulu"
	case '1':
		return "One"
	case '2':
		return "Two"
	case '3':
		return "Three"
	case '4':
		return "Four"
	case '5':
		return "Five"
	case '6':
		return "Six"
	case '7':
		return "Seven"
	case '8':
		return "Eight"
	case '9':
		return "Nine"
	case '0':
		return "Zero"
	} //switch

	return ""
} //func

// Use will register this TransRune function for the acrophonetic.TransRune.
func Use() {
	acrophonetic.RegisterFunc(TransRune)
} //func
