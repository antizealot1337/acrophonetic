package nato

import "testing"

func TestTransRune(t *testing.T) {

	trepr := func(expected string, chars ...rune) {
		for _, c := range chars {
			if actual := TransRune(c); actual != expected {
				t.Errorf(`Expected repr for %c to be "%s" but was "%s"`,
					c, expected, actual)

			} //if
		} //for
	} //func

	trepr("Alfa", 'a', 'A')
	trepr("Bravo", 'b', 'B')
	trepr("Charlie", 'c', 'C')
	trepr("Delta", 'd', 'D')
	trepr("Echo", 'e', 'E')
	trepr("Foxtrot", 'f', 'F')
	trepr("Golf", 'g', 'G')
	trepr("Hotel", 'h', 'H')
	trepr("India", 'i', 'I')
	trepr("Juliett", 'j', 'J')
	trepr("Kilo", 'k', 'K')
	trepr("Lima", 'l', 'L')
	trepr("Mike", 'm', 'M')
	trepr("November", 'n', 'N')
	trepr("Oscar", 'o', 'O')
	trepr("Papa", 'p', 'P')
	trepr("Quebec", 'q', 'Q')
	trepr("Romeo", 'r', 'R')
	trepr("Sierra", 's', 'S')
	trepr("Tango", 't', 'T')
	trepr("Uniform", 'u', 'U')
	trepr("Victor", 'v', 'V')
	trepr("Whiskey", 'w', 'W')
	trepr("Xray", 'x', 'X')
	trepr("Yankee", 'y', 'Y')
	trepr("Zulu", 'z', 'z')
	trepr("One", '1')
	trepr("Two", '2')
	trepr("Three", '3')
	trepr("Four", '4')
	trepr("Five", '5')
	trepr("Six", '6')
	trepr("Seven", '7')
	trepr("Eight", '8')
	trepr("Nine", '9')
	trepr("Zero", '0')
	trepr("", rune(0))
} //func
